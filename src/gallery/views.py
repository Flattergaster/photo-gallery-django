from itertools import islice

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse_lazy, reverse
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic.edit import CreateView

from gallery.tasks import do_thumbnail
from .forms import PhotoForm
from .models import Photo


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'index.html'

    def dispatch(self, request, *args, **kwargs):
        has_perm = self.request.user.has_perm

        if(
            not has_perm('gallery.view_own_photos') and
            not has_perm('gallery.view_all_photos')
        ):
            return self.handle_no_permission()
        return super(IndexView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        has_perm = self.request.user.has_perm
        if has_perm('gallery.view_all_photos'):
            photos = Photo.objects.all()
        elif has_perm('gallery.view_own_photos'):
            photos = Photo.objects.filter(owner=self.request.user)
        else:
            raise PermissionDenied()

        context = super(IndexView, self).get_context_data(**kwargs)

        rows = []
        photos = photos.iterator()
        while True:
            row = tuple(islice(photos, 3))
            rows.append(row)
            if len(row) < 3:
                break

        context.update({
            'rows': rows,
        })
        return context


class AddPhotoView(PermissionRequiredMixin, CreateView):
    form_class = PhotoForm
    template_name = 'add.html'
    permission_required = 'gallery.add_photo'
    raise_exception = True
    permission_denied_message = 'You can not add photos'

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(AddPhotoView, self).form_valid(form)

    def get_success_url(self):
        do_thumbnail(self.object.url, self.object.id, (256, 256))
        return reverse('index')


class EditPhotoView(PermissionRequiredMixin, UpdateView):
    form_class = PhotoForm
    model = Photo
    template_name = 'edit.html'
    success_url = reverse_lazy('index')
    permission_required = 'gallery.change_photo'
    raise_exception = True


class DeletePhotoView(PermissionRequiredMixin, DeleteView):
    model = Photo
    template_name = 'confirm_delete.html'
    success_url = reverse_lazy('index')
    permission_required = 'gallery.delete_photo'
    raise_exception = True
